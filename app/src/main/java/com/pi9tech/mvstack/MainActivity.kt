package com.pi9tech.mvstack

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pi9tech.mvstack.databinding.ActivityMainBinding
import com.pi9tech.mvstack.model.NowPlaying
import com.pi9tech.mvstack.ui.view.NowPlayingListAdapter
import com.pi9tech.mvstack.ui.view.NowPlayingViewModel

class MainActivity : AppCompatActivity() {


    private lateinit var mainActivityBinding: ActivityMainBinding
    private lateinit var viewModel: NowPlayingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, NowPlayingVMFactory(this)).get(NowPlayingViewModel::class.java)

        //data binding
        mainActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mainActivityBinding.recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        mainActivityBinding.recyclerView.setHasFixedSize(true)
        mainActivityBinding.lifecycleOwner = this
        mainActivityBinding.viewModel = viewModel
        viewModel.nowPlaying.observe(this, Observer<PagedList<NowPlaying>> {
            Log.d("MainActivity", "list size: ${it?.size}")

            viewModel.nowPlayingListAdapter.submitList(it)
        })
    }
}

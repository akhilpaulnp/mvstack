package com.pi9tech.mvstack

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.pi9tech.mvstack.databinding.ActivityMovieDetailBinding
import com.pi9tech.mvstack.model.MovieDetail
import com.pi9tech.mvstack.model.NowPlaying
import com.pi9tech.mvstack.ui.view.MovieDetailViewModel

class MovieDetailActivity : AppCompatActivity() {

    private lateinit var viewModel: MovieDetailViewModel
    private lateinit var binding: ActivityMovieDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, MovieDetailVMFactory(this)).get(MovieDetailViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_detail)
        viewModel.fetchMovieDetail(getMovieIdFromIntent())
        with(binding)
        {
            lifecycleOwner = this@MovieDetailActivity
        }
        viewModel.movieDetail.observe(this, Observer<MovieDetail> {
            Log.d("MovieDetailActivity", "Movie title: ${it?.title}")
            binding.movie = it
        })
    }

    // method for getting movie id from intent
    private fun getMovieIdFromIntent(): Int {
        return intent.getIntExtra(movieId, 0)
    }

    // for starting movie detail activity on now playing list click
    companion object {
        private const val movieId = "movie_id"
        fun startActivity(context: Context?, nowPlaying: NowPlaying) {
            val i = Intent(context, MovieDetailActivity::class.java)
            i.putExtra(movieId, nowPlaying.id)
            context?.startActivity(i)
        }
    }

}

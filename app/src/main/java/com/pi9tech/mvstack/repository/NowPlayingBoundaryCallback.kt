package com.pi9tech.mvstack.repository

import android.util.Log
import androidx.paging.PagedList
import com.pi9tech.mvstack.api.NowPlayingResponse
import com.pi9tech.mvstack.api.TheMDBApi
import com.pi9tech.mvstack.model.NowPlaying
import com.pi9tech.mvstack.room.NowPlayingDao
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor

// pagination boundary call back class
class NowPlayingBoundaryCallback(private val nowPlayingDao: NowPlayingDao, private val theMDBApi: TheMDBApi, private val ioExecutor: Executor) : PagedList.BoundaryCallback<NowPlaying>() {

    private var lastRequestPage = 1

    private var isRequestInProgress = false

    private fun loadAndSaveData() {
        Log.d("NPBoundaryCallback", "loadAndSaveData")

        if (isRequestInProgress) return

        isRequestInProgress = true

        theMDBApi.fetchNowPlayingMovies(lastRequestPage).enqueue(object : Callback<NowPlayingResponse> {
            override fun onFailure(call: Call<NowPlayingResponse>?, t: Throwable) {
                Log.d("NPBoundaryCallback", "fail to get data")
                isRequestInProgress = false
            }

            override fun onResponse(call: Call<NowPlayingResponse>?, response: Response<NowPlayingResponse>) {
                Log.d("NPBoundaryCallback", "got a response $response")
                if (response.isSuccessful) {
                    val repos = response.body()?.results ?: emptyList()
//                    onSuccess(repos)
                    Log.d("NPBoundaryCallback", "list size ${repos.size}")
                    for (movie in repos) {
                        movie.page = lastRequestPage
                    }
                    ioExecutor.execute {
                        nowPlayingDao.insertNowPlayingMovieList(repos)
                        lastRequestPage++
                        isRequestInProgress = false
                    }

                } else {
                    isRequestInProgress = false
                }
            }
        })

    }

    override fun onZeroItemsLoaded() {
        Log.d("NPBoundaryCallback", "onZeroItemsLoaded")
        loadAndSaveData()
    }

    override fun onItemAtEndLoaded(itemAtEnd: NowPlaying) {
        Log.d("NPBoundaryCallback", "onItemAtEndLoaded")
        loadAndSaveData()
    }
}
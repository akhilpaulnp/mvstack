package com.pi9tech.mvstack.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.pi9tech.mvstack.api.TheMDBApi
import com.pi9tech.mvstack.model.MovieDetail
import com.pi9tech.mvstack.model.NowPlaying
import com.pi9tech.mvstack.room.MVStackDb
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor

// repository class for getting data from network and database
class TheMDBRepository(val db: MVStackDb, private val theMDBApi: TheMDBApi, private val ioExecutor: Executor) {

    companion object {
        private const val DATABASE_PAGE_SIZE = 20
    }

    fun loadNowPlayingMovies(): LiveData<PagedList<NowPlaying>> {
        Log.d("TheMDBRepository", "Loading movies")

        // Get data from the local cache
        val dataSourceFactory = db.movieDao().getNowPlayingMovieList()

        val boundaryCallback = NowPlayingBoundaryCallback(db.movieDao(), theMDBApi, ioExecutor)

        val pagingConfig = PagedList.Config.Builder().setPrefetchDistance(10).setPageSize(DATABASE_PAGE_SIZE).build()

        return LivePagedListBuilder(dataSourceFactory, pagingConfig)
            .setBoundaryCallback(boundaryCallback)
            .build()

    }

    fun loadMovieDetail(movieId: Int): LiveData<MovieDetail> {
        Log.d("TheMDBRepository", "Loading movie detail")

        val movieDetail = db.movieDetailDao().getMovieDetail(movieId)
        if (movieDetail.value != null) {
            return movieDetail
        } else {
            theMDBApi.fetchMovieDetail(movieId).enqueue(object : Callback<MovieDetail> {
                override fun onFailure(call: Call<MovieDetail>?, t: Throwable) {
                    Log.d("TheMDBRepository", "fail to get data ${t.message}")
                }

                override fun onResponse(call: Call<MovieDetail>?, response: Response<MovieDetail>) {
                    Log.d("TheMDBRepository", "got a response ${response.isSuccessful}")
                    if (response.isSuccessful) {
                        val movieDetail1 = response.body()
                        if (movieDetail1 != null) {
                            Log.d("TheMDBRepository", "got a movieDetail1 response ${movieDetail1.original_title}")
                            ioExecutor.execute {
                                db.movieDetailDao().insertMovieDetail(movieDetail1)
                            }
                        }
                    } else {
                        Log.d("TheMDBRepository", "fail to get data")
                    }
                }
            })
            return db.movieDetailDao().getMovieDetail(movieId)
        }

    }// this method can be improved
}
package com.pi9tech.mvstack.room

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pi9tech.mvstack.model.NowPlaying

// now playing movie list room db end point
@Dao
interface NowPlayingDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNowPlayingMovieList(nowPlaying: List<NowPlaying>)

    @Query("SELECT * FROM NowPlaying ORDER BY page ASC")
    fun getNowPlayingMovieList(): DataSource.Factory<Int, NowPlaying>
}
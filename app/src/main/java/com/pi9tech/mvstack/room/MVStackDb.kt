package com.pi9tech.mvstack.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.pi9tech.mvstack.model.MovieDetail
import com.pi9tech.mvstack.model.NowPlaying

// room database create class
@Database(entities = [(NowPlaying::class), (MovieDetail::class)], version = 1, exportSchema = false)
@TypeConverters(value = [(StringListConverter::class), (IntegerListConverter::class)])
abstract class MVStackDb : RoomDatabase() {
    companion object {
        fun create(context: Context): MVStackDb {
            val databaseBuilder = Room.databaseBuilder(context, MVStackDb::class.java, "MVStack.db")

            return databaseBuilder.fallbackToDestructiveMigration().build()
        }
    }

    abstract fun movieDao(): NowPlayingDao

    abstract fun movieDetailDao(): MovieDetailDao
}
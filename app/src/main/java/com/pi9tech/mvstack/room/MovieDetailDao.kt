package com.pi9tech.mvstack.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pi9tech.mvstack.model.MovieDetail

// movie detail room db end point
@Dao
interface MovieDetailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovieDetail(movie: MovieDetail)

    @Query("SELECT * FROM MovieDetail WHERE id = :id_")
    fun getMovieDetail(id_: Int): LiveData<MovieDetail>

}

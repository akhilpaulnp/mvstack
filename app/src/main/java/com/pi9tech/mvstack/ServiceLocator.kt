package com.pi9tech.mvstack

import android.app.Application
import android.content.Context
import com.pi9tech.mvstack.api.TheMDBApi
import com.pi9tech.mvstack.repository.TheMDBRepository
import com.pi9tech.mvstack.room.MVStackDb
import java.util.concurrent.Executor
import java.util.concurrent.Executors

interface ServiceLocator {
    companion object {
        private val Lock = Any()
        private var instance: ServiceLocator? = null
        fun instance(context: Context): ServiceLocator {
            synchronized(Lock) {
                if (instance == null) {
                    instance = DefaultServiceLocator(app = context.applicationContext as Application)
                }
                return instance!!
            }
        }
    }

    fun getRepository(): TheMDBRepository

    fun getNetworkExecutor(): Executor

    fun getDiskIOExecutor(): Executor

    fun getTheMDBApi(): TheMDBApi
}

// default for providing repository and other necessary end point to the application
open class DefaultServiceLocator(val app: Application) : ServiceLocator {

    private val diskIO = Executors.newSingleThreadExecutor()

    private val networkIO = Executors.newFixedThreadPool(5)

    private val db by lazy { MVStackDb.create(app) }

    private val api by lazy { TheMDBApi.create() }

    override fun getRepository(): TheMDBRepository = TheMDBRepository(db = db, theMDBApi = api, ioExecutor = getDiskIOExecutor())

    override fun getNetworkExecutor(): Executor = networkIO

    override fun getDiskIOExecutor(): Executor = diskIO

    override fun getTheMDBApi(): TheMDBApi = api
}
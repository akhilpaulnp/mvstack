package com.pi9tech.mvstack.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

// room data holder class for now playing movie list
@Entity(primaryKeys = [("id")])
data class NowPlaying(
    var page: Int,
    @SerializedName("popularity") val popularity: Float,
    @SerializedName("vote_count") val vote_count: Int,
    @SerializedName("video") val video: Boolean,
    @SerializedName("poster_path") val poster_path: String?,
    @SerializedName("id") val id: Int,
    @SerializedName("adult") val adult: Boolean,
    @SerializedName("backdrop_path") val backdrop_path: String?,
    @SerializedName("original_language") val original_language: String,
    @SerializedName("original_title") val original_title: String,
    @SerializedName("genre_ids") val genre_ids: List<Int>,
    @SerializedName("title") val title: String,
    @SerializedName("vote_average") val vote_average: Float,
    @SerializedName("overview") val overview: String,
    @SerializedName("release_date") val release_date: String
)


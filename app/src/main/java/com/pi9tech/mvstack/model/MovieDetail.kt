package com.pi9tech.mvstack.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

// room data holder class for movie detail
@Entity(primaryKeys = [("id")])
data class MovieDetail(
    @SerializedName("adult") val adult: Boolean,
    @SerializedName("backdrop_path") val backdrop_path: String?,
//    @SerializedName("belongs_to_collection") val belongs_to_collection: String?, // excluded for later implementation
    @SerializedName("budget") val budget: Long,
//    @SerializedName("genres") val genres: List<Genres>, // excluded for later implementation
    @SerializedName("homepage") val homepage: String?,
    @SerializedName("id") val id: Int,
    @SerializedName("imdb_id") val imdb_id: String?,
    @SerializedName("original_language") val original_language: String,
    @SerializedName("original_title") val original_title: String,
    @SerializedName("overview") val overview: String?,
    @SerializedName("popularity") val popularity: Float,
    @SerializedName("poster_path") val poster_path: String?,
//    @SerializedName("production_companies") val production_companies: List<Production_companies>, // excluded for later implementation
//    @SerializedName("production_countries") val production_countries: List<Production_countries>, // excluded for later implementation
    @SerializedName("release_date") val release_date: String,
    @SerializedName("revenue") val revenue: Int,
    @SerializedName("runtime") val runtime: Int?,
//    @SerializedName("spoken_languages") val spoken_languages: List<Spoken_languages>, // excluded for later implementation
    @SerializedName("status") val status: String,
    @SerializedName("tagline") val tagline: String?,
    @SerializedName("title") val title: String,
    @SerializedName("video") val video: Boolean,
    @SerializedName("vote_average") val vote_average: Float,
    @SerializedName("vote_count") val vote_count: Int
)
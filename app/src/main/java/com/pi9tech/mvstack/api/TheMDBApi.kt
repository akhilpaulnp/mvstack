package com.pi9tech.mvstack.api

import com.pi9tech.mvstack.model.MovieDetail
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

// the movie db api end point class for network request
interface TheMDBApi {

    // method for getting now playing movie list
    @GET("/3/movie/now_playing")
    fun fetchNowPlayingMovies(@Query("page") page: Int): Call<NowPlayingResponse>


    // method for getting movie detail based on movie id
    @GET("/3/movie/{movie_id}")
    fun fetchMovieDetail(@Path("movie_id") id: Int): Call<MovieDetail>

    companion object {
        private const val BASE_URL = "https://api.themoviedb.org"

        fun create(): TheMDBApi {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(RequestInterceptor())

                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
                .create(TheMDBApi::class.java)
        }
    }
}
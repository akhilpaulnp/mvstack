package com.pi9tech.mvstack.api

import com.pi9tech.mvstack.model.NowPlaying

// data holder class for now playing movie list response
data class NowPlayingResponse(
    val page: Int,
    val results: List<NowPlaying> = emptyList(),
    val total_results: Int,
    val total_pages: Int
)
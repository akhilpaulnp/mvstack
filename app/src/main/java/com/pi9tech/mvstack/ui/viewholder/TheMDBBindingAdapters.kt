package com.pi9tech.mvstack.ui.viewholder

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions

// Binding methods for loading movie poster and backdrop images
@BindingAdapter("loadPoster")
fun bindLoadPoster(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
            .load("https://image.tmdb.org/t/p/w342$imageUrl")
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))//image cache
            .into(view)
    }
}

@BindingAdapter("loadBackDrop")
fun bindLoadBackDrop(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
            .load("https://image.tmdb.org/t/p/w780$imageUrl")
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))//image cache
            .into(view)
    }
}
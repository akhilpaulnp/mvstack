package com.pi9tech.mvstack.ui.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.pi9tech.mvstack.model.MovieDetail
import com.pi9tech.mvstack.repository.TheMDBRepository

// view model class for movie detail activity
class MovieDetailViewModel(private val repository: TheMDBRepository) : ViewModel() {
    lateinit var movieDetail: LiveData<MovieDetail>

    fun fetchMovieDetail(id: Int) {
        movieDetail = repository.loadMovieDetail(id)
    }

}
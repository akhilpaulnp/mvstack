package com.pi9tech.mvstack.ui.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.pi9tech.mvstack.model.NowPlaying
import com.pi9tech.mvstack.repository.TheMDBRepository

// view model class for now playing movies list - main activity
class NowPlayingViewModel(repository: TheMDBRepository) : ViewModel() {

    var nowPlaying: LiveData<PagedList<NowPlaying>> = repository.loadNowPlayingMovies()

    val nowPlayingListAdapter = NowPlayingListAdapter()

}
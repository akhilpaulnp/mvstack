package com.pi9tech.mvstack.ui.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.pi9tech.mvstack.R
import com.pi9tech.mvstack.databinding.ListItemMovieBinding
import com.pi9tech.mvstack.model.NowPlaying
import com.pi9tech.mvstack.ui.viewholder.NowPlayingListViewHolder

// recycler adapter for listing the now playing movies with pagination
class NowPlayingListAdapter : PagedListAdapter<NowPlaying, NowPlayingListViewHolder>(MOVIE_COMPARATOR) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NowPlayingListViewHolder {
        val binding: ListItemMovieBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.list_item_movie, parent, false)
        return NowPlayingListViewHolder(binding = binding)
    }

    override fun onBindViewHolder(holder: NowPlayingListViewHolder, position: Int) {
        getItem(position)?.let { holder.bindData(it) }
    }

    companion object {
        private val MOVIE_COMPARATOR = object : DiffUtil.ItemCallback<NowPlaying>() {
            override fun areItemsTheSame(oldItem: NowPlaying, newItem: NowPlaying): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: NowPlaying, newItem: NowPlaying): Boolean =
                oldItem.original_title == newItem.original_title && oldItem.vote_average == newItem.vote_average && oldItem.vote_count == newItem.vote_count && oldItem.release_date == newItem.release_date && oldItem.popularity == newItem.popularity
        }
    }
}
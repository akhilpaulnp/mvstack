package com.pi9tech.mvstack.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.pi9tech.mvstack.MovieDetailActivity
import com.pi9tech.mvstack.databinding.ListItemMovieBinding
import com.pi9tech.mvstack.model.NowPlaying

class NowPlayingListViewHolder(private val binding: ListItemMovieBinding) : RecyclerView.ViewHolder(binding.root) {


    fun bindData(nowPlaying: NowPlaying) {

        binding.movie = nowPlaying

        // on list click starting movie detail activity
        itemView.setOnClickListener {
            MovieDetailActivity.startActivity(binding.root.context, nowPlaying)
        }
    }


}
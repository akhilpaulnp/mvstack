package com.pi9tech.mvstack

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pi9tech.mvstack.ui.view.NowPlayingViewModel

// class and method for create and initialize now playing list view model
class NowPlayingVMFactory(private val activity: AppCompatActivity) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NowPlayingViewModel::class.java)) {
            val repo = ServiceLocator.instance(activity).getRepository()
            @Suppress("UNCHECKED_CAST")
            return NowPlayingViewModel(repository = repo) as T
        }
        throw IllegalArgumentException("This view model class is not accepted.")
    }

}
package com.pi9tech.mvstack

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pi9tech.mvstack.ui.view.MovieDetailViewModel

// class and method for create and initialize movie detail view model
class MovieDetailVMFactory(private val activity: AppCompatActivity) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieDetailViewModel::class.java)) {
            val repo = ServiceLocator.instance(activity).getRepository()
            @Suppress("UNCHECKED_CAST")
            return MovieDetailViewModel(repository = repo) as T
        }
        throw IllegalArgumentException("This view model class is not accepted.")
    }

}